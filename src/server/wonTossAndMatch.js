function wonTossAndMatch(matches) {
  let allTeams = new Set();
  let resultWonTossAndMatch = {};
  matches.map((obj) => {
    allTeams.add(obj.team1, obj.team2);
  });

  allTeams.forEach((teamName) => {
    let count = 0;
    matches.map((obj) => {
      if (obj.toss_winner == teamName && obj.winner == teamName) {
        count++;
      }
    });
    resultWonTossAndMatch[teamName] = count;
  });
  return resultWonTossAndMatch;
}

module.exports=wonTossAndMatch
