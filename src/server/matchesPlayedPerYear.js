function matchesPlayedPerYear(matches) {
  let yearsObj = {};
  matches.map((obj) => {
    if (yearsObj[obj.season] === undefined) {
      yearsObj[obj.season] = 1;
    } else {
      yearsObj[obj.season]++;
    }
  });
  return yearsObj;
}

module.exports = matchesPlayedPerYear;
