const csv = require("csvtojson");
const csv2 = require("csvtojson");
const matchesPath = "../data/matches.csv";
const deliveriesPath = "../data/deliveries.csv";

const matchesPlayedPerYear =require('./matchesPlayedPerYear.js')
const allYearsWonPerTeam =  require('./matchesWonPerTeamPerYear.js')
const extraRunsConceded = require('./extraRunsConceded.js')
const topEconomicalBowlers = require('./topEconomicalBowlers')
const wonTossAndMatch =  require('./wonTossAndMatch.js')
const playerOfMatchPerSeason=require('./playerOfMatchPerSeason')
const bowlerSuperOverEconomy = require('./bowlerSuperOverEconomy')
const batsmanStrikeRatePerSeason = require('./batsmanStrikeRatePerSeason')
const playerDismiss =require('./playerDismiss')

const fs = require("fs");

csv()
  .fromFile(matchesPath)
  .then((matches) => {
    csv2()
      .fromFile(deliveriesPath)
      .then((deliveries) => {
        const resultMatchesPlayedPerYear = matchesPlayedPerYear(matches);
        const path1 = "../public/output/matchesPlayedPerYear.json";
        convertToJson(path1, resultMatchesPlayedPerYear);

        const resultAllYearsWonPerTeam = allYearsWonPerTeam(matches);
        const path2 = "../public/output/matchesWonPerTeamPerYear.json";
        convertToJson(path2, resultAllYearsWonPerTeam);

        const resultExtraRunsConceded = extraRunsConceded(
          matches,
          deliveries,
          2016
        );
        const path3 = "../public/output/extraRunsConceded.json";
        convertToJson(path3, resultExtraRunsConceded);

        let resultTopEconomicalBowlers = topEconomicalBowlers(
          matches,
          deliveries,
          2015
        );
        const path4 = "../public/output/topEconomicalBowlers.json";
        convertToJson(path4, resultTopEconomicalBowlers);

        const resultWonTossAndMatch = wonTossAndMatch(matches);
        const path5 = "../public/output/wonTossAndMatch.json";
        convertToJson(path5, resultWonTossAndMatch);

        const resultPlayerOfMatchPerSeason =
        playerOfMatchPerSeason(matches);
        const path6 = "../public/output/playerOfMatchPerSeason.json";
        convertToJson(path6, resultPlayerOfMatchPerSeason);

        const resultBowlerSuperOverEconomy= bowlerSuperOverEconomy(deliveries);
        const path7 = "../public/output/bowlerSuperOverEconomy.json";
        convertToJson(path7, resultBowlerSuperOverEconomy);

        const resultPlayerDismiss = playerDismiss(deliveries);
        const path8 = "../public/output/playerDismiss.json";
        convertToJson(path8, resultPlayerDismiss);


        let player = "MS Dhoni";
        const resultBatsmanStrikeRatePerSeason = batsmanStrikeRatePerSeason(matches, deliveries,player);
        const path9 = "../public/output/batsmanStrikeRatePerSeason.json";
        convertToJson(path9, resultBatsmanStrikeRatePerSeason);
      });
  });

function convertToJson(path, name) {
  fs.writeFileSync(path, JSON.stringify(name), (err) => {
    if (err) console.log(err);
  });
}
