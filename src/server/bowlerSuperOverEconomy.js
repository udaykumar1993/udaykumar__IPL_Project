function bowlerSuperOverEconomy(deliveries) {
  let topEconomicalBowler = {};
  let bowlers = [];
  let bowlersScores = {};
  deliveries
    .filter((item) => item.is_super_over == 1)
    .map((item) => {
      if (!bowlers.includes(item.bowler)) {
        bowlers.push(item.bowler);
        bowlersScores[item.bowler] = findScore(item.bowler);
      }
    });
  function findScore(bowler) {
    let score = 0;
    let balls = 0;
    deliveries
      .filter((item) => item.bowler == bowler && item.is_super_over == 1)
      .map((item) => {      
          score += Number(item.total_runs);
          balls++;
        
      });
    let economy = (score / (balls / 6)).toFixed(2);
    return Number(economy);
  }

  let topEconomy = Math.min(...Object.values(bowlersScores));
  Object.keys(bowlersScores).map((bowler) => {
    if (bowlersScores[bowler] === topEconomy) {
      topEconomicalBowler[bowler] = topEconomy;
    }
  });

  return topEconomicalBowler;
}
module.exports = bowlerSuperOverEconomy;
