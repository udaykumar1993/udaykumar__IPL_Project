function playerOfMatchPerSeason(matches) {
  let allPlayerPerSeason = {};
  matches.map((item) => {
    if (allPlayerPerSeason[item.season] === undefined) {
      allPlayerPerSeason[item.season] = findPlayer(item.season);
    }
  });
  function findPlayer(season) {
    let awardedPlayers = {};
    let awardPlayerPerSeason = {};
    matches
      .filter((item) => item.season == season)
      .map((item) => {
        if (awardedPlayers[item.player_of_match] == undefined) {
          awardedPlayers[item.player_of_match] = 1;
        } else {
          awardedPlayers[item.player_of_match]++;
        }
      });
    let max = Math.max(...Object.values(awardedPlayers));
    Object.keys(awardedPlayers).map((player) => {
      if (awardedPlayers[player] == max) {
        awardPlayerPerSeason[player] = max;
      }
    });
    return awardPlayerPerSeason;
  }
  return allPlayerPerSeason;
}

module.exports = playerOfMatchPerSeason;
