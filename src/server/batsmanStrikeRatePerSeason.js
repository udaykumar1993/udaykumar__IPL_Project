function batsmanStrikeRatePerSeason(matches, deliveries, player) {
  let seasons = [];
  let strikRate = {};
  let resultPlayerStrikeRate = {};

  matches.map((item) => {
    if (!seasons.includes(item.season)) {
      seasons.push(item.season);
      strikRate[item.season] = findSeason(item.season);
    }
  });
  function findSeason(season) {
    let seasonStrikeRate = 0;
    let totalRuns = 0;
    let totalBalls = 0;
    let ids = matches
      .filter((item) => item.season == season)
      .map((item) => item.id);
    deliveries.map((item) => {
      if (ids.includes(item.match_id) && item.batsman == player) {
        totalRuns += Number(item.batsman_runs);
        totalBalls++;
      }
    });
    seasonStrikeRate = Number((totalRuns / totalBalls) * 100).toFixed(2);
    return seasonStrikeRate;
  }
  resultPlayerStrikeRate[player] = strikRate;
  return resultPlayerStrikeRate;
}

module.exports = batsmanStrikeRatePerSeason;
