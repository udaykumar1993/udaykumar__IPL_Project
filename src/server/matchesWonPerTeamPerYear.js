function allYearsWonPerTeam(matches) {
  let winnersResult = {};
  matches.map((obj) => {
    if (winnersResult[obj.season] === undefined)
      winnersResult[obj.season] = matchesWon(obj.season);
  });
  function matchesWon(year) {
    let winners = {};
    matches.map((item) => {
      if (item.season === year) {
        if (winners[item.winner] === undefined) {
          winners[item.winner] = 1;
        } else {
          winners[item.winner]++;
        }
      }
    });
    return winners;
  }
  return winnersResult;
}
module.exports = allYearsWonPerTeam;
