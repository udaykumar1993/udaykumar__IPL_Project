function playerDismiss(deliveries) {
  let disMissedPlayers = [];
  deliveries.map((object) => {
    if (
      !disMissedPlayers.includes(object.batsman) &&
      object.player_dismissed != ""
    ) {
      disMissedPlayers.push(object.player_dismissed);
    }
  });

  let playerDismissedByBowler = [];

  disMissedPlayers.map((player) => {
    let bowlersArray = [];
    deliveries.map((object) => {
      if (object.player_dismissed == player) {
        bowlersArray.push(object.bowler);
      }
    });
    let playerTimes = {};
    bowlersArray.map((bowler) => {
      if (playerTimes[bowler] === undefined) {
        playerTimes[bowler] = 1;
      } else {
        playerTimes[bowler]++;
      }
    });

    let max = Math.max(...Object.values(playerTimes));
    let maxTimesDismissed = {};
    Object.keys(playerTimes).map((bowler) => {
      if (playerTimes[bowler] == max) {
        maxTimesDismissed[bowler] = max;
      }
    });

    let bowlerNames = "";
    let NumberOfTimes = 0;
    Object.keys(maxTimesDismissed).map((name) => {
      if (maxTimesDismissed[name] > NumberOfTimes) {
        NumberOfTimes = maxTimesDismissed[name];
        bowlerNames = name;
      }
    });

    playerDismissedByBowler.push([player, bowlerNames, NumberOfTimes]);
  });

  let resultDismissedPlayer = "";
  let anotherPlayer = "";
  let numberOfTimes = 0;

  playerDismissedByBowler.map((item) => {
    if (numberOfTimes < item[2]) {
      numberOfTimes = item[2];
      resultDismissedPlayer = item[0];
      anotherPlayer = item[1];
    }
  });

  let tempObj = {};
  tempObj[resultDismissedPlayer] = { anotherPlayer, numberOfTimes };
  return tempObj;
}

module.exports = playerDismiss