function extraRunsConceded(matches, deliveries, year) {
  let matchIds2016 = matches
    .filter((match) => match.season == year)
    .map((match) => match.id);
  return extraRuns(matchIds2016,deliveries);
}

function extraRuns(matchIds2016,deliveries) {
  let extraRuns = {};
  deliveries.map((item) => {
    if (matchIds2016.includes(item.match_id)) {
      if (extraRuns[item.bowling_team] === undefined) {
        extraRuns[item.bowling_team] = parseInt(item.extra_runs, 10);
      } else {
        extraRuns[item.bowling_team] += parseInt(item.extra_runs, 10);
      }
    }
  });
  return extraRuns;
}

module.exports = extraRunsConceded;
