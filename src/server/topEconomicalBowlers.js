function topEconomicalBowlers(matches, deliveries, season) {
  let economic = {};
  let resultEconomicalBowlers = {};
  let matchIds2015 = matches
    .filter((match) => match.season == season)
    .map((match) => match.id);
  deliveries.map((object) => {
    if (matchIds2015.includes(object.match_id)) {
      if (economic[object.bowler] == undefined) {
        economic[object.bowler] =Number(object.total_runs)
      } else {
        economic[object.bowler] +=Number(object.total_runs)
      }
    }
  });
  Object.keys(economic).map((bowler) => {
    economic[bowler] =
      Math.round((economic[bowler] / over(bowler)) * 100) / 100;
  });

  function over(bowler) {
    let balls = 0;
    deliveries.map((obj) => {
      if (matchIds2015.includes(obj.match_id) && obj.bowler == bowler) {
        balls++;
      }
    });
    return balls / 6;
  }

  let allBowlers = Object.keys(economic).map((key) => [key, economic[key]]);
  allBowlers.sort((a, b) => a[1] - b[1]);
  allBowlers.slice(0, 10).map((item) => {
    resultEconomicalBowlers[item[0]] = item[1];
  });
  return resultEconomicalBowlers;
}

module.exports = topEconomicalBowlers;
